from operator import attrgetter

from ryu.app import simple_switch_13
from ryu.controller import ofp_event
# --------------------
from ryu.app import traffic_monitoring
from ryu.controller import event
# --------------------

from ryu.controller.handler import MAIN_DISPATCHER, DEAD_DISPATCHER
from ryu.controller.handler import set_ev_cls
from ryu.lib import hub

class TrafficCollector13(traffic_monitoring.SimpleMonitor13):
    
    def __init__(self, *args, **kwargs):
        super(TrafficCollector13, self).__init__(*args, **kwargs)
        self.f = open("collector.txt", "+a")
    
    @set_ev_cls(ofp_event.EventOFPStateChange,
                [MAIN_DISPATCHER, DEAD_DISPATCHER])
    def _state_change_handler_report(self, ev):
        datapath = ev.datapath
        self.f.write("/r==STATE CHANGE HANDLER REPORT==/n")
        self.f.write("register datapath: %016x" % datapath.id)
        self._state_change_handler(ev)

    ## ip find based on switch datapath
    @set_ev_cls(event.EventSwitchEnter)
    def switch_features_handler(self, ev):
        ddress = ev.switch.dp.address
        dpid = ev.switch.dp.id
        print "Ip adress " + dpid
        self.f.write("Ip adress " + dpid)
    
    def _monitor(self):
        while True:
            for dp in self.datapaths.values():
                self._request_stats(dp)
            hub.sleep(3)
