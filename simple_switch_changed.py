# Copyright (C) 2011 Nippon Telegraph and Telephone Corporation.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from operator import attrgetter
from ryu.base import app_manager
from ryu.controller import ofp_event
from ryu.controller.handler import CONFIG_DISPATCHER, MAIN_DISPATCHER, DEAD_DISPATCHER 
from ryu.controller.handler import set_ev_cls
from ryu.ofproto import ofproto_v1_3
from ryu.lib.packet import packet
from ryu.lib.packet import ethernet
from ryu.lib.packet import ether_types
from ryu.lib.packet import ipv4
from ryu.lib.packet import icmp
from ryu.lib.packet import arp
from ryu.lib.packet import icmp
from ryu.lib.packet import tcp
from ryu.lib.packet import udp
from ryu.lib import hub
import time

class SimpleSwitch13(app_manager.RyuApp):
    OFP_VERSIONS = [ofproto_v1_3.OFP_VERSION]

    def __init__(self, *args, **kwargs):
        super(SimpleSwitch13, self).__init__(*args, **kwargs)
        self.mac_to_port = {}
        self.pkt_count = 0
        self.count = 0
        self.sum_time_pkt = 0
        self.min_time_pkt = 99999999
        self.max_time_pkt = -1
        self.pkt_in = []
        #traffic monitoring
        self.datapaths = {}
        self.monitor_thread = hub.spawn(self._monitor)
        self.FlowStats = [[0]*100 for i in range(100)]
        self.FlowCount = [[0]*100 for i in range(100)]        
        self.FlowPKT = [[0]*100 for i in range(100)]       
        self.min_FLOWPKT = [9999 for i in range (10000)]
        self.max_FLOWPKT = [0 for i in range (10000)]        
        self.count_pkt = 0
        self.FlowSum = 0
        

    @set_ev_cls(ofp_event.EventOFPSwitchFeatures, CONFIG_DISPATCHER)
    def switch_features_handler(self, ev):
        datapath = ev.msg.datapath
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser
        

        # install table-miss flow entry
        #
        # We specify NO BUFFER to max_len of the output action due to
        # OVS bug. At this moment, if we specify a lesser number, e.g.,
        # 128, OVS will send Packet-In with invalid buffer_id and
        # truncated packet data. In that case, we cannot output packets
        # correctly.  The bug has been fixed in OVS v2.1.0.
        match = parser.OFPMatch()
        actions = [parser.OFPActionOutput(ofproto.OFPP_CONTROLLER,
                                          ofproto.OFPCML_NO_BUFFER)]
        self.add_flow(datapath, 0, match, actions)

    def add_flow(self, datapath, priority, match, actions, buffer_id=None):
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        inst = [parser.OFPInstructionActions(ofproto.OFPIT_APPLY_ACTIONS,
                                             actions)]
        if buffer_id:
            mod = parser.OFPFlowMod(datapath=datapath, buffer_id=buffer_id,
                                    priority=priority, match=match,
                                    instructions=inst)
        else:
            mod = parser.OFPFlowMod(datapath=datapath, priority=priority,
                                    match=match, instructions=inst)
        datapath.send_msg(mod)
    
    #######################function to calculate SD#############################
    def mean(self, data):
        """Return the sample arithmetic mean of data."""
        n = len(data)
        if n < 1:
            raise ValueError('mean requires at least one data point')
        return sum(data)/n # in Python 2 use sum(data)/float(n)
    def _ss(self, data):
        """Return sum of square deviations of sequence data."""
        c = self.mean(data)
        ss = sum((x-c)**2 for x in data)
        return ss    
        
    def stddev(self, data, ddof=0):
        """Calculates the population standard deviation
        by default; specify ddof=1 to compute the sample
        standard deviation."""
        n = len(data)
        if n < 2:
            raise ValueError('variance requires at least two data points')
        ss = self._ss(data)
        pvar = ss/(n-ddof)
        return pvar**0.5
    ############################################################################
    @set_ev_cls(ofp_event.EventOFPPacketIn, MAIN_DISPATCHER)
    def _packet_in_handler(self, ev):
        time_in_pkt = time.time()        
        # If you hit this you might want to increase
        # the "miss_send_length" of your switch
        if ev.msg.msg_len < ev.msg.total_len:
            self.logger.debug("packet truncated: only %s of %s bytes",
                              ev.msg.msg_len, ev.msg.total_len)
        msg = ev.msg
        datapath = msg.datapath
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser
        in_port = msg.match['in_port']

        pkt = packet.Packet(msg.data)
        eth = pkt.get_protocols(ethernet.ethernet)[0]
        #======================+========================================
        pkt_arp = pkt.get_protocol(arp.arp)
        pkt_ipv4 = pkt.get_protocol(ipv4.ipv4)
        pkt_icmp = pkt.get_protocol(icmp.icmp)
        pkt_tcp = pkt.get_protocol(tcp.tcp)
        pkt_udp = pkt.get_protocol(udp.udp)
        dpid = datapath.id
        dst = eth.dst
        src = eth.src
        
        
        self.logger.info("=================== PKT ANALISYS =================================")        
        self.mac_to_port.setdefault(dpid, {})        
        self.logger.info("DATAPATH ID: %s", dpid)
        self.logger.info("SRC PORT: %s", src)
        self.logger.info("DST PORT: %s", dst)
        self.logger.info("PORT: %s", in_port)     
        print "---------------------------------------------------"
        print "ICMP pack captured:" 
        print pkt_icmp
        print "---------------------------------------------------"
        print "Arp pack captured:"
        print pkt_arp
        print "---------------------------------------------------"
        print "Ipv4 pack captured:"
        print pkt_ipv4                    
        print "---------------------------------------------------"
        print "TCP pack captured:"
        print pkt_tcp                    
        print "---------------------------------------------------"
        print "UDP pack captured:"
        print pkt_udp
        self.logger.info("==================================================================")
        #======================+++======================================
        if eth.ethertype == ether_types.ETH_TYPE_LLDP:
            # ignore lldp packet
            return
        
        # learn a mac address to avoid FLOOD next time.
        self.mac_to_port[dpid][src] = in_port

        if dst in self.mac_to_port[dpid]:
            out_port = self.mac_to_port[dpid][dst]
        else:
            out_port = ofproto.OFPP_FLOOD

        actions = [parser.OFPActionOutput(out_port)]

        # install a flow to avoid packet_in next time
        if out_port != ofproto.OFPP_FLOOD:
            match = parser.OFPMatch(in_port=in_port, eth_dst=dst, eth_src=src, ip_proto=124)
            # verify if we have a valid buffer_id, if yes avoid to send both
            # flow_mod & packet_out
            if msg.buffer_id != ofproto.OFP_NO_BUFFER:
                self.add_flow(datapath, 1, match, actions, msg.buffer_id)
                return
            else:
                self.add_flow(datapath, 1, match, actions)
        data = None
        if msg.buffer_id == ofproto.OFP_NO_BUFFER:
            data = msg.data

        out = parser.OFPPacketOut(datapath=datapath, buffer_id=msg.buffer_id,
                                  in_port=in_port, actions=actions, data=data)
        
        
        datapath.send_msg(out)
        """------------------------------------------------------------------"""        
        time_out_pkt = time.time()
        
        diff_time = time_out_pkt - time_in_pkt
        self.pkt_in.append(diff_time)
        self.sum_time_pkt = self.sum_time_pkt + diff_time
        self.logger.info("=====================TIME ANALISYS================================")
        print "time pack enter: ", time_in_pkt
        print "time pack out: ", time_out_pkt
        print "pack diff time: ", diff_time
        self.count = self.count + 1        
        print "Media time between packs: ", self.sum_time_pkt/self.count
        self.min_time_pkt = min(self.min_time_pkt, diff_time)
        self.max_time_pkt = max(self.max_time_pkt, diff_time)
        print "Max diff time: ", self.max_time_pkt 
        print "Min diff time: ", self.min_time_pkt
        print "Time Standart Deviation: ", self.stddev(self.pkt_in, 1)
        """------------------------------------------------------------------"""
        print "=================================================================="
################################################################################
    @set_ev_cls(ofp_event.EventOFPStateChange,
                [MAIN_DISPATCHER, DEAD_DISPATCHER])
    def _state_change_handler(self, ev):
        datapath = ev.datapath
        if ev.state == MAIN_DISPATCHER:
            if datapath.id not in self.datapaths:
                self.logger.debug('register datapath: %016x', datapath.id)
                self.datapaths[datapath.id] = datapath
        elif ev.state == DEAD_DISPATCHER:
            if datapath.id in self.datapaths:
                self.logger.debug('unregister datapath: %016x', datapath.id)
                del self.datapaths[datapath.id]

    def _monitor(self):
        while True:
            for dp in self.datapaths.values():
                self._request_stats(dp)
            hub.sleep(10)

    def _request_stats(self, datapath):
        self.logger.debug('send stats request: %016x', datapath.id)
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        req = parser.OFPFlowStatsRequest(datapath)
        datapath.send_msg(req)

        req = parser.OFPPortStatsRequest(datapath, 0, ofproto.OFPP_ANY)
        datapath.send_msg(req)

    @set_ev_cls(ofp_event.EventOFPFlowStatsReply, MAIN_DISPATCHER)
    def _flow_stats_reply_handler(self, ev):
        body = ev.msg.body
       
        self.logger.info('datapath         '
                         'in-port  eth-dst           '
                         'out-port packets  bytes')
        self.logger.info('---------------- '
                         '-------- ----------------- '
                         '-------- -------- --------')
        for stat in sorted([flow for flow in body if flow.priority == 1],
                           key=lambda flow: (flow.match['in_port'],
                                             flow.match['eth_dst'])):
            self.logger.info('%016x %8x %17s %8x %8d %8d',
                             ev.msg.datapath.id,
                            stat.match['in_port'], stat.match['eth_dst'],
                             stat.instructions[0].actions[0].port,
                            stat.packet_count, stat.byte_count)
            
            self.logger.info("====================PKT FLOW ANALISYS=================================")            
            print "FLOW ",stat.match['in_port'], "-----------------------> ", stat.instructions[0].actions[0].port
            p1 = stat.match['in_port']
            p2 = stat.instructions[0].actions[0].port
            self.FlowStats[p1][p2] += stat.packet_count
            self.FlowCount[p1][p2] += 1     
            pkt_flow = self.FlowStats[p1][p2]
            pkt_flow_count = self.FlowCount[p1][p2]
            self.FlowPKT[2*p1 + p2].append(stat.packet_count)
            var = self.FlowPKT[2*p1 + p2]                           
            print "FLOW MEDIA PKT: ", pkt_flow/pkt_flow_count
            self.min_FLOWPKT[2*p1 + p2] = min(self.min_FLOWPKT[2*p1 + p2], stat.packet_count)            
            self.max_FLOWPKT[2*p1 + p2] = max(self.max_FLOWPKT[2*p1 + p2], stat.packet_count)
            print "MIN MEDIA PKT/S FLOW: ", self.min_FLOWPKT[2*p1 + p2]
            print "MAX MEDIA PKT/S FLOW: ", self.max_FLOWPKT[2*p1 + p2]
            print "PKT MEDIA STANDART DEVIATION: ", self.stddev(var, 1)
            self.FlowSum += stat.packet_count
            self.count_pkt += 1
            print "GERAL MEDIA PKT/S PER FLOW: ", self.FlowSum/self.count_pkt
            self.logger.info("=====================================================")
            
                            
          
    @set_ev_cls(ofp_event.EventOFPPortStatsReply, MAIN_DISPATCHER)
    def _port_stats_reply_handler(self, ev):
        body = ev.msg.body
        
        self.logger.info('datapath         port     '
                         'rx-pkts  rx-bytes rx-error '
                         'tx-pkts  tx-bytes tx-error')
        self.logger.info('---------------- -------- '
                        '-------- -------- -------- '
                         '-------- -------- --------')
        for stat in sorted(body, key=attrgetter('port_no')):
            self.logger.info('%016x %8x %8d %8d %8d %8d %8d %8d',
                             ev.msg.datapath.id, stat.port_no,
                             stat.rx_packets, stat.rx_bytes, stat.rx_errors,
                             stat.tx_packets, stat.tx_bytes, stat.tx_errors) 
                      
        
